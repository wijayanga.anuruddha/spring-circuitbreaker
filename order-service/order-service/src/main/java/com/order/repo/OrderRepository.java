package com.order.repo;

import com.order.model.Order;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;


public interface OrderRepository extends CrudRepository<Order, Long>, JpaSpecificationExecutor<Order>  {

}
