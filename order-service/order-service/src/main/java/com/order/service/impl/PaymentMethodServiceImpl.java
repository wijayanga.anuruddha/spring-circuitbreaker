package com.order.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.order.model.PaymentTypeDTO;
import com.order.service.PaymentMethodService;
import com.order.service.PaymentMethodServiceProxy;
import com.order.util.PaymentTypeEnum;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodServiceImpl implements PaymentMethodService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodServiceImpl.class);

    private final PaymentMethodServiceProxy paymentMethodServiceProxy;

    @HystrixCommand(fallbackMethod = "fallbackRetrievePaymentType")
    @Override
    public PaymentTypeDTO getPaymentType(PaymentTypeEnum typeEnum) {

        LOGGER.debug("Return PaymentMethod for type :: {} ", typeEnum );
        return paymentMethodServiceProxy.retrievePaymentType(typeEnum);
    }

    @SuppressWarnings("unused")
    public PaymentTypeDTO fallbackRetrievePaymentType(PaymentTypeEnum typeEnum) {

        LOGGER.debug("Return default fallback method for PaymentMethod :: {} ", typeEnum );
        return PaymentTypeDTO.builder()
                .id(5)
                .type(PaymentTypeEnum.PAYTM.name())
                .build();
    }
}
