package com.order.service.impl;

import com.order.exception.NoDataFoundException;
import com.order.model.Order;
import com.order.model.OrderDTO;
import com.order.model.PaymentType;
import com.order.model.PaymentTypeDTO;
import com.order.repo.OrderRepository;
import com.order.service.OrderService;
import com.order.service.PaymentMethodService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    private final OrderRepository orderRepository;
    private final PaymentMethodService paymentMethodService;

    @Override
    public void saveOrder(OrderDTO orderDTO) {

        LOGGER.info("save order :: orderName - {}", orderDTO.getOrderName());

        PaymentTypeDTO paymentTypeDTO = paymentMethodService.getPaymentType(orderDTO.getType());
        LOGGER.info("Get payment type for type :: {}", orderDTO.getType());

        if(Objects.isNull(paymentTypeDTO)){
            LOGGER.info("No paymentMethod found for type :: {}", orderDTO.getType());
            throw new NoDataFoundException("No data found for given paymentType");
        }

        orderDTO.setPaymentType(populatePaymentType(paymentTypeDTO));

        orderRepository.save(populateOrder(orderDTO));
    }

    @Override
    public List<OrderDTO> getOrders() {

        Iterable<Order> orders = orderRepository.findAll();
        LOGGER.info("Get orders");
        return populateOrderDTO(orders);
    }

    private Order populateOrder(OrderDTO orderDTO) {
        return Order.builder()
                .orderName(orderDTO.getOrderName())
                .description(orderDTO.getDescription())
                .paymentType(orderDTO.getPaymentType())
                .build();
    }

    private PaymentType populatePaymentType(PaymentTypeDTO paymentTypeDTO) {
        return PaymentType.builder()
                .id(paymentTypeDTO.getId())
                .type(paymentTypeDTO.getType())
                .build();
    }

    private List<OrderDTO> populateOrderDTO(Iterable<Order> orders) {

        List<OrderDTO> orderDTOS = new ArrayList<>();
        for (Order order : orders) {
            orderDTOS.add(OrderDTO.builder()
                    .id(order.getId())
                    .orderName(order.getOrderName())
                    .description(order.getDescription())
                    .paymentType(order.getPaymentType())
                    .build()
            );
        }
        return orderDTOS;
    }
}
