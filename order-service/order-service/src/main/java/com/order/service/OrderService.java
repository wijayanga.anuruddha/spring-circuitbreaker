package com.order.service;

import com.order.model.OrderDTO;

import java.util.List;

public interface OrderService {

    void saveOrder (OrderDTO orderDTO);

    List<OrderDTO> getOrders ();
}
