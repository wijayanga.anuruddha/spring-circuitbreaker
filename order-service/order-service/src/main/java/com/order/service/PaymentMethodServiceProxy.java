package com.order.service;

import com.order.model.PaymentTypeDTO;
import com.order.util.PaymentTypeEnum;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "payment-method", url = "localhost:8081")
public interface PaymentMethodServiceProxy {

    @GetMapping("/v1/payment/{type}")
    PaymentTypeDTO retrievePaymentType(@PathVariable PaymentTypeEnum type);
}
