package com.order.service;

import com.order.model.PaymentTypeDTO;
import com.order.util.PaymentTypeEnum;

public interface PaymentMethodService {

    PaymentTypeDTO getPaymentType(PaymentTypeEnum typeEnum);
}
