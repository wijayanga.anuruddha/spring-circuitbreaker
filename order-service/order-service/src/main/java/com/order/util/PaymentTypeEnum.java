package com.order.util;

public enum PaymentTypeEnum {

    CASH, VISA, MASTER, AMEX, PAYTM
}
