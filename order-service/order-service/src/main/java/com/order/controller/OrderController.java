package com.order.controller;

import com.order.model.OrderDTO;
import com.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/order")
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    private final OrderService orderService;

    @PostMapping(value = "", consumes = { "application/json", "application/xml" },
            produces = { "application/json", "application/xml" })
    public ResponseEntity<Void> createOrder (@RequestBody @Valid OrderDTO orderDTO){

        orderService.saveOrder(orderDTO);
        LOGGER.debug("create order for orderName :: {}", orderDTO.getOrderName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "")
    public ResponseEntity<List<OrderDTO>> retrieveOrder (){

        List<OrderDTO> orders = orderService.getOrders();
        LOGGER.debug("Load all orders");
        return new ResponseEntity<>(orders,HttpStatus.OK);
    }
}
