package com.order.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentTypeDTO {

    private int id;
    private String type;
}
