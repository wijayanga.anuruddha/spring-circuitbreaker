package com.order.model;

import com.order.util.PaymentTypeEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderDTO {

    private int id;
    private String orderName;
    private String description;
    private PaymentType paymentType;
    private PaymentTypeEnum type;
}
