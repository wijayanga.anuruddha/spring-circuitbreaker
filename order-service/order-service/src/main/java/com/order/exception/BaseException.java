package com.order.exception;

public class BaseException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String field;
	private String value;

	public BaseException(final Throwable cause) {
		super(cause);
		
	}
	
	public BaseException(final String message) {
		super(message);
	}
	
	public BaseException(final String message, final String field, final String value) {
		super(message);
		this.field= field;
		this.value=value;
	}

	
	public BaseException(final Throwable cause, final String field, final String value) {
		super(cause);
		this.field= field;
		this.value=value;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
