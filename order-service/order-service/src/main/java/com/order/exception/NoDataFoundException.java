package com.order.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class NoDataFoundException extends BaseException{

	private static final long serialVersionUID = 1293511374050329171L;

	public NoDataFoundException(String message) {
		super(message);
	}
}
