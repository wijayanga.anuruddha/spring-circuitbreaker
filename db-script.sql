CREATE TABLE `order-service`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `payment_type_id` INT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `order-service`.`payment_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
