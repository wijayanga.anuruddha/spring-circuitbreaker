package com.payment.service.impl;

import com.payment.model.PaymentType;
import com.payment.model.PaymentTypeDTO;
import com.payment.repo.PaymentMethodRepository;
import com.payment.service.PaymentMethodService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodServiceImpl implements PaymentMethodService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodServiceImpl.class);

    private final PaymentMethodRepository paymentMethodRepository;

    @Override
    public PaymentTypeDTO getPaymentMethod(String type) {

        PaymentType paymentType = paymentMethodRepository.findByType(type);
        LOGGER.info("Get payment type for type :: {} ", type);
        return populatePaymentType(paymentType);

    }

    private PaymentTypeDTO populatePaymentType (PaymentType paymentType){

        return PaymentTypeDTO.builder()
                .id(paymentType.getId())
                .type(paymentType.getType())
                .build();
    }
}
