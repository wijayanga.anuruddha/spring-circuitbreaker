package com.payment.service;

import com.payment.model.PaymentTypeDTO;

public interface PaymentMethodService {

    PaymentTypeDTO getPaymentMethod (String type);
}
