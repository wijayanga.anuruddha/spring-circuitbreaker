package com.payment.repo;

import com.payment.model.PaymentType;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;


public interface PaymentMethodRepository extends CrudRepository<PaymentType, Long>, JpaSpecificationExecutor<PaymentType>  {

    PaymentType findByType (String type);
}
