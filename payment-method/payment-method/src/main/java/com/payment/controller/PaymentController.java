package com.payment.controller;

import com.payment.model.PaymentTypeDTO;
import com.payment.service.PaymentMethodService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/payment")
public class PaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    private final PaymentMethodService paymentMethodService;

    @GetMapping(value = "/{type}")
    public ResponseEntity<PaymentTypeDTO> getPaymentType(@PathVariable String type) {

        PaymentTypeDTO paymentMethod = paymentMethodService.getPaymentMethod(type);
        LOGGER.debug("Load payment method for type :: {}", type);
        return new ResponseEntity<>(paymentMethod, HttpStatus.OK);
    }
}
